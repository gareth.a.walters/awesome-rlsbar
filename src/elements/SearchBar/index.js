import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { resultsContainer, searchBar, searchBarWrapper } from './searchBar.module.scss';

const SearchBar = ({ searchTerm, setSearchTerm, results, dropdown, ...rest }) => {
  const [searchActive, setSearchActive] = useState(false);

  return (
    <div
      className={searchBarWrapper}
      onFocus={() => setSearchActive(true)}
      onBlur={(e) => {
        if (!e.currentTarget.contains(e.relatedTarget)) {
          setSearchActive(false);
        }
      }}
    >
      <input
        type='text'
        value={searchTerm}
        className={searchBar}
        onChange={(e) => setSearchTerm(e.target.value)}
        {...rest}
      />
      {dropdown && results && searchActive && searchTerm !== '' && (
        <div className={resultsContainer}>
          {results.map((result, i) => (
            <Link
              to={`/drink/${result.idDrink}`}
              onClick={() => {
                setSearchTerm('');
                setSearchActive(false);
              }}
              key={i}
            >
              <img src={result.strDrinkThumb} alt={result.strDrink} />
              <span>{result.strDrink}</span>
            </Link>
          ))}
        </div>
      )}
    </div>
  );
};

SearchBar.propTypes = {
  searchTerm: PropTypes.string,
  setSearchTerm: PropTypes.func,
  results: PropTypes.array,
  dropdown: PropTypes.bool,
};

export default SearchBar;
