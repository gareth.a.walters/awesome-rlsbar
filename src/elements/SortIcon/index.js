import React from 'react';
import PropTypes from 'prop-types';
import { ReactComponent as Icon } from '../../assets/icons/sort.svg';
import { selected, sortIconWrapper } from './sortIcon.module.scss';

const SortIcon = ({ sort, sortReverse, sortedState }) => {
  return (
    <div className={sortIconWrapper}>
      <Icon className={!sortedState ? null : selected} onClick={sortReverse} />
      <Icon className={!sortedState ? selected : null} onClick={sort} />
    </div>
  );
};

SortIcon.propTypes = {
  sort: PropTypes.func,
  sortReverse: PropTypes.func,
  sortedState: PropTypes.bool,
};

export default SortIcon;
