import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Category from './pages/Category';
import Cocktails from './pages/Cocktails';
import DrinkDetails from './pages/DrinkDetails';
import Home from './pages/Home';
import IngredientDetails from './pages/IngredientDetails';
import IngredientList from './pages/IngredientList';
import NotFound from './pages/NotFound';
import Todo from './pages/Todo';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path='/' component={Home} />
        <Route exact path='/cocktails' component={Cocktails} />
        <Route exact path='/drink/:id' component={DrinkDetails} />
        <Route exact path='/ingredient/:name' component={IngredientDetails} />
        <Route exact path='/ingredients' component={IngredientList} />
        <Route exact path='/category/:name' component={Category} />
        <Route exact path='/todo' component={Todo} />
        <Route component={NotFound} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
