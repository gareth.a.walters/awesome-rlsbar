import React, { useEffect, useState } from 'react';
import { Nav, Navbar, NavDropdown } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { getCategories, searchDrinkByName } from '../../api.js';
import SearchBar from '../../elements/SearchBar';
import { linksWrapper, navbar } from './header.module.scss';

const Header = () => {
  const [categories, setCategories] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [drinks, setDrinks] = useState(null);

  useEffect(() => {
    async function getDrinkCategories() {
      const categoriesArr = await getCategories();
      setCategories(categoriesArr);
    }
    getDrinkCategories();
  }, []);

  useEffect(() => {
    async function searchDrinks(searchTerm) {
      const drinksArr = await searchDrinkByName(searchTerm);
      setDrinks(drinksArr);
    }
    searchDrinks(searchTerm);
  }, [searchTerm]);

  return (
    <Navbar className={navbar} variant='dark' expand='lg'>
      <Navbar.Brand href='/'>RedLight &lt;Bar&gt;</Navbar.Brand>
      <Navbar.Toggle aria-controls='basic-navbar-nav' />
      <Navbar.Collapse id='basic-navbar-nav'>
        <Nav className={linksWrapper}>
          <LinkContainer to='/cocktails'>
            <Nav.Link>Cocktails</Nav.Link>
          </LinkContainer>
          <LinkContainer to='/ingredients'>
            <Nav.Link>Ingredients</Nav.Link>
          </LinkContainer>
          <NavDropdown title='Categories' id='basic-nav-dropdown'>
            {categories &&
              categories.map((category, i) => (
                <NavDropdown.Item key={i} href={`/category/${category.replaceAll('/', '%2F')}`}>
                  {category}
                </NavDropdown.Item>
              ))}
          </NavDropdown>
          <LinkContainer to='/todo'>
            <Nav.Link>Todo</Nav.Link>
          </LinkContainer>
        </Nav>
        <SearchBar
          searchTerm={searchTerm}
          setSearchTerm={setSearchTerm}
          placeholder='Search all cocktails'
          results={drinks}
          dropdown
        />
      </Navbar.Collapse>
    </Navbar>
  );
};

export default Header;
