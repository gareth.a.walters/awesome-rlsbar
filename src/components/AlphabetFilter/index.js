import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { alphabetFilterContainer, selected } from './alphabetFilter.module.scss';

const AlphabetFilter = ({ selectedLetter, handleLetterClick }) => {
  const [alphabet, setAlphabet] = useState([]);

  useEffect(() => {
    const letters = Array.from(Array(26))
      .map((_, i) => i + 65)
      .map((l) => String.fromCharCode(l));

    setAlphabet(letters);
  }, []);

  return (
    <div className={alphabetFilterContainer}>
      {alphabet &&
        alphabet.map((letter, i) => (
          <button key={i} onClick={() => handleLetterClick(letter)}>
            <span className={`${selectedLetter === letter ? selected : null}`}>{letter}</span>
          </button>
        ))}
    </div>
  );
};

AlphabetFilter.propTypes = {
  selectedLetter: PropTypes.string,
  handleLetterClick: PropTypes.func,
};

export default AlphabetFilter;
