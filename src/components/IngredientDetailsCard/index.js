import React from 'react';
import PropTypes from 'prop-types';
import { cardContainer, detailsWrapper, typeABVWrapper } from './ingredientDetailsCard.module.scss';

const IngredientDetailsCard = ({ details, image }) => {
  return (
    <>
      {details && (
        <div className={cardContainer}>
          <img src={image} alt={details.strIngredient} />
          <div className={detailsWrapper}>
            <h2>{details.strIngredient}</h2>
            <div className={typeABVWrapper}>
              {details.strABV ? (
                <div>
                  <h4>ABV:</h4>
                  <p>{details.strABV}%</p>
                </div>
              ) : null}
              {details.strType ? (
                <div>
                  <h4>Type:</h4>
                  <p>{details.strType}</p>
                </div>
              ) : null}
            </div>
            <h4>Description:</h4>
            {details.strDescription ? (
              <p>{details.strDescription}</p>
            ) : (
              <p>No desription available</p>
            )}
          </div>
        </div>
      )}
    </>
  );
};

IngredientDetailsCard.propTypes = {
  details: PropTypes.object,
  image: PropTypes.string,
};

export default IngredientDetailsCard;
