import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { cardContainer } from './drinkDetailsCard.module.scss';

const DrinkDetailsCard = ({ details }) => {
  const [measuresAndIngredients, setMeasuresAndIngredients] = useState([]);

  useEffect(() => {
    // Combine drink measures and ingredients into array of objects
    const getMeasuresAndIngredients = () => {
      if (details) {
        const measuresArr = Object.keys(details)
          .filter((key) => key.indexOf('Measure') !== -1)
          .filter((key) => details[key] !== null && details[key] !== '')
          .map((key) => details[key]);

        const ingredientsArr = Object.keys(details)
          .filter((key) => key.indexOf('Ingredient') !== -1)
          .filter((key) => details[key] !== null && details[key] !== '')
          .map((key) => details[key]);

        const measureAndIngredientArr = ingredientsArr.map((ing, i) => {
          return { ingredient: ing, measure: measuresArr[i] };
        });

        setMeasuresAndIngredients(measureAndIngredientArr);
      }
    };
    getMeasuresAndIngredients();
  }, [details]);

  return (
    <div className={cardContainer}>
      <img src={details.strDrinkThumb} alt={details.strDrink} />
      <div>
        <h2>{details.strDrink}</h2>
        <h4>Ingredients:</h4>
        <ul>
          {measuresAndIngredients &&
            measuresAndIngredients.map((el, i) => (
              <li key={i}>
                <Link to={`/ingredient/${el.ingredient}`}>
                  {el.measure} {el.ingredient}
                </Link>
              </li>
            ))}
        </ul>
        <h4>Instructions:</h4>
        <p>{details.strInstructions}</p>
        <h4>Serving Glass:</h4>
        <p>{details.strGlass}</p>
        <h4>Category:</h4>
        <p>{details.strCategory}</p>
      </div>
    </div>
  );
};

DrinkDetailsCard.propTypes = {
  details: PropTypes.object,
};

export default DrinkDetailsCard;
