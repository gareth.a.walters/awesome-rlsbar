import React from 'react';
import PropTypes from 'prop-types';
import { Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { cardButton } from './drinkCard.module.scss';

const DrinkCard = ({ drink }) => {
  return (
    <Card className='text-center'>
      <Card.Img variant='top' src={drink.strDrinkThumb} />
      <Card.Body>
        <Card.Title>{drink.strDrink}</Card.Title>
      </Card.Body>
      <Card.Footer className={cardButton}>
        <Link to={`/drink/${drink.idDrink}`}>
          <Button variant='outline-secondary'>View Drink</Button>
        </Link>
      </Card.Footer>
    </Card>
  );
};

DrinkCard.propTypes = {
  drink: PropTypes.object,
};

export default DrinkCard;
