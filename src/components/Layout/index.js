import React from 'react';
import PropTypes from 'prop-types';
import { Container } from 'react-bootstrap';
import Header from '../Header';

const Layout = ({ hideNav, children }) => {
  return (
    <>
      {!hideNav && <Header />}
      <Container className='p-3'>{children}</Container>
    </>
  );
};

Layout.propTypes = {
  hideNav: PropTypes.bool,
  children: PropTypes.node,
};

export default Layout;
