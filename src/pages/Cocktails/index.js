import React, { useEffect, useState } from 'react';
import { Spinner } from 'react-bootstrap';
import { getDrinksByLetter } from '../../api';
import AlphabetFilter from '../../components/AlphabetFilter';
import DrinkCard from '../../components/DrinkCard';
import Layout from '../../components/Layout';
import { desktopTitle, drinksGrid, filterGridWrapper, mobileTitle } from './cocktails.module.scss';

const Cocktails = () => {
  const [loading, setLoading] = useState(false);
  const [selectedLetter, setSelectedLetter] = useState('A');
  const [drinks, setDrinks] = useState([]);

  useEffect(() => {
    async function getData() {
      setLoading(true);
      const drinks = await getDrinksByLetter(selectedLetter);
      setDrinks(drinks);
      setLoading(false);
    }
    getData();
  }, [selectedLetter]);

  const handleLetterClick = (value) => {
    setSelectedLetter(value);
  };

  return (
    <Layout>
      <div>
        <h2 className={desktopTitle}>Browse Cocktails</h2>
        <div className={filterGridWrapper}>
          <AlphabetFilter selectedLetter={selectedLetter} handleLetterClick={handleLetterClick} />
          <div>
            <h2 className={mobileTitle}>Browse Cocktails</h2>
            <div className={drinksGrid}>
              {loading && (
                <Spinner animation='border' role='status' className='mx-auto'>
                  <span className='sr-only'>Loading...</span>
                </Spinner>
              )}
              {!loading && drinks && drinks.map((drink, i) => <DrinkCard drink={drink} key={i} />)}
            </div>
            {!loading && !drinks && (
              <p>No drinks beginning with letter &quot;{selectedLetter}&quot;</p>
            )}
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Cocktails;
