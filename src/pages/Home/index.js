import React, { useEffect, useState } from 'react';
import { Button, Spinner } from 'react-bootstrap';
import { getRandomDrink } from '../../api';
import DrinkDetailsCard from '../../components/DrinkDetailsCard';
import Layout from '../../components/Layout';
import { titleButtonWrapper } from './home.module.scss';

const Home = () => {
  const [loading, setLoading] = useState(false);
  const [details, setDetails] = useState(null);

  useEffect(() => {
    getRandDrink();
  }, []);

  //GET random drink
  const getRandDrink = async () => {
    setLoading(true);
    const drink = await getRandomDrink();
    setDetails(drink);
    setLoading(false);
  };

  return (
    <Layout>
      <div>
        <div className={titleButtonWrapper}>
          <h2>Random drink of the day:</h2>
          <Button variant='outline-secondary' onClick={() => getRandDrink()}>
            Try another
          </Button>
        </div>
        {loading && (
          <Spinner animation='border' role='status' className='mx-auto'>
            <span className='sr-only'>Loading...</span>
          </Spinner>
        )}
        {!loading && details && <DrinkDetailsCard details={details} />}
      </div>
    </Layout>
  );
};

export default Home;
