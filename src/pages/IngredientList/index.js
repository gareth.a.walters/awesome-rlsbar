import React, { useEffect, useState } from 'react';
import { Spinner } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { getIngredients } from '../../api';
import Layout from '../../components/Layout';
import SearchBar from '../../elements/SearchBar';
import SortIcon from '../../elements/SortIcon';
import {
  filterWrapper,
  ingredientListWrapper,
  titleFilterWrapper,
} from './ingredientList.module.scss';

const IngredientsList = () => {
  const [loading, setLoading] = useState(false);
  const [alphabeticalSortReverse, setAlphabeticalSortReverse] = useState(false);
  const [ingredients, setIngredients] = useState(null);
  const [ingredientsFiltered, setIngredientsFiltered] = useState(null);
  const [searchTerm, setSearchTerm] = useState('');

  useEffect(() => {
    const getIngredientList = async () => {
      setLoading(true);
      const ingredientsArr = await getIngredients();
      if (ingredientsArr && ingredientsArr.length > 0) {
        setIngredients(ingredientsArr);
        setIngredientsFiltered(ingredientsArr);
      }
      setLoading(false);
    };
    getIngredientList();
  }, []);

  useEffect(() => {
    const handleSearch = () => {
      setIngredientsFiltered(
        ingredients &&
          ingredients.filter((ingredient) => {
            if (searchTerm === '') {
              return ingredient;
            } else if (ingredient.toLowerCase().includes(searchTerm.toLowerCase())) {
              return ingredient;
            } else {
              return null;
            }
          })
      );
    };
    handleSearch();
  }, [searchTerm, ingredients]);

  const sortAlphabetically = () => {
    setAlphabeticalSortReverse(false);
  };

  const sortAlphabeticallyReverse = () => {
    setAlphabeticalSortReverse(true);
  };

  return (
    <Layout>
      <div className={titleFilterWrapper}>
        <h2>Ingredients List</h2>
        <div className={filterWrapper}>
          <SearchBar
            searchTerm={searchTerm}
            setSearchTerm={setSearchTerm}
            placeholder='Search ingredients'
          />
          <SortIcon
            sort={sortAlphabetically}
            sortReverse={sortAlphabeticallyReverse}
            sortedState={alphabeticalSortReverse}
          />
        </div>
      </div>

      <div className={ingredientListWrapper}>
        {loading && (
          <Spinner animation='border' role='status' className='mx-auto'>
            <span className='sr-only'>Loading...</span>
          </Spinner>
        )}
        <ul>
          {!loading &&
            ingredientsFiltered &&
            ingredientsFiltered
              .sort((a, b) => (!alphabeticalSortReverse ? a.localeCompare(b) : b.localeCompare(a)))
              .map((ingredient, i) => (
                <li key={i}>
                  <Link to={`/ingredient/${ingredient}`}>{ingredient}</Link>
                </li>
              ))}
        </ul>
        {!loading && ingredientsFiltered && ingredientsFiltered.length <= 0 && (
          <p>No ingredients matching search</p>
        )}
      </div>
    </Layout>
  );
};

export default IngredientsList;
