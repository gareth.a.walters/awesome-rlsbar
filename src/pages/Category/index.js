import React, { useEffect, useState } from 'react';
import { Spinner } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { getDrinkInCategory } from '../../api';
import DrinkCard from '../../components/DrinkCard';
import Layout from '../../components/Layout';
import SearchBar from '../../elements/SearchBar';
import SortIcon from '../../elements/SortIcon';
import { drinksGrid, filterWrapper, titleFilterWrapper } from './category.module.scss';

const Categories = () => {
  const [loading, setLoading] = useState(false);
  const [alphabeticalSortReverse, setAlphabeticalSortReverse] = useState(false);
  const [drinks, setDrinks] = useState(null);
  const [drinksFiltered, setDrinksFiltered] = useState(null);
  const [searchTerm, setSearchTerm] = useState('');
  const { name } = useParams();

  useEffect(() => {
    const getDrinksByCategory = async () => {
      setLoading(true);
      const drinksArr = await getDrinkInCategory(name);
      if (drinksArr && drinksArr.length > 0) {
        setDrinks(drinksArr);
        setDrinksFiltered(drinksArr);
      }
      setLoading(false);
    };
    getDrinksByCategory();
  }, [name]);

  useEffect(() => {
    const handleSearch = () => {
      setDrinksFiltered(
        drinks &&
          drinks.filter((drink) => {
            if (searchTerm === '') {
              return drink;
            } else if (drink.strDrink.toLowerCase().includes(searchTerm.toLowerCase())) {
              return drink;
            } else {
              return null;
            }
          })
      );
    };
    handleSearch();
  }, [searchTerm, drinks]);

  const sortAlphabetically = () => {
    setAlphabeticalSortReverse(false);
  };

  const sortAlphabeticallyReverse = () => {
    setAlphabeticalSortReverse(true);
  };

  return (
    <Layout>
      <div className={titleFilterWrapper}>
        <h2>{name.replaceAll('%2F', '/')}</h2>
        <div className={filterWrapper}>
          <SearchBar
            value={searchTerm}
            placeholder={`Search in ${name.replaceAll('%2F', '/')}`}
            onChange={(e) => setSearchTerm(e.target.value)}
          />
          <SortIcon
            sort={sortAlphabetically}
            sortReverse={sortAlphabeticallyReverse}
            sortedState={alphabeticalSortReverse}
          />
        </div>
      </div>
      <div className={drinksGrid}>
        {loading && (
          <Spinner animation='border' role='status' className='mx-auto'>
            <span className='sr-only'>Loading...</span>
          </Spinner>
        )}
        {!loading &&
          drinksFiltered &&
          drinksFiltered
            .sort((a, b) =>
              !alphabeticalSortReverse
                ? a.strDrink.localeCompare(b.strDrink)
                : b.strDrink.localeCompare(a.strDrink)
            )
            .map((drink, i) => <DrinkCard drink={drink} key={i} />)}
      </div>
      {!loading && drinksFiltered && drinksFiltered.length <= 0 && <p>No drinks matching search</p>}
    </Layout>
  );
};

export default Categories;
