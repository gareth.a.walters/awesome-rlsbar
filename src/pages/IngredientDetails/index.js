import React, { useEffect, useState } from 'react';
import { Spinner } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { getIngredientByName, getIngredientImage, searchDrinksByIngredient } from '../../api';
import DrinkCard from '../../components/DrinkCard';
import IngredientDetailsCard from '../../components/IngredientDetailsCard';
import Layout from '../../components/Layout';
import { drinksGrid } from './ingredientDetails.module.scss';

const IngredientDetails = () => {
  const [loading, setLoading] = useState(false);
  const [details, setDetails] = useState(null);
  const [image, setImage] = useState();
  const [drinks, setDrinks] = useState(null);
  const { name } = useParams();

  useEffect(() => {
    // GET ingredient details
    async function getIngredient() {
      setLoading(true);
      const ingredient = await getIngredientByName(name);
      if (ingredient && ingredient.length > 0) {
        setDetails(ingredient[0]);
      }
      setLoading(false);
    }
    // GET ingredient image
    async function getImage() {
      setLoading(true);
      const url = await getIngredientImage(name);
      setImage(url);
      setLoading(false);
    }
    // GET drink matching ingredient
    async function getDrinksByIngredient() {
      setLoading(true);
      const matchingDrinks = await searchDrinksByIngredient(name);
      setDrinks(matchingDrinks);
      setLoading(false);
    }
    getIngredient(name);
    getImage(name);
    getDrinksByIngredient(name);
  }, [name]);

  return (
    <Layout>
      <div>
        {loading && (
          <Spinner animation='border' role='status' className='mx-auto'>
            <span className='sr-only'>Loading...</span>
          </Spinner>
        )}
        {!loading && details && <IngredientDetailsCard details={details} image={image} />}

        {!loading && details && <h5>Drinks containing {details.strIngredient}:</h5>}
        <div className={drinksGrid}>
          {loading && (
            <Spinner animation='border' role='status' className='mx-auto'>
              <span className='sr-only'>Loading...</span>
            </Spinner>
          )}
          {!loading && drinks && drinks.map((drink, i) => <DrinkCard drink={drink} key={i} />)}
        </div>
        {!loading && !details && <p>No ingredient with that name</p>}
      </div>
    </Layout>
  );
};

export default IngredientDetails;
