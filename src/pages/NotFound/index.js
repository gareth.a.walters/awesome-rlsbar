import React from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import noAlcohol from '../../assets/images/no_alcohol.svg';
import Layout from '../../components/Layout';
import { pageContainer } from './notFound.module.scss';

const NotFound = () => {
  return (
    <Layout hideNav>
      <div className={pageContainer}>
        <h1>This is an alcohol free zone</h1>
        <img src={noAlcohol} alt='' />
        <Link to={'/'}>
          <Button variant='outline-secondary'>Back to the bar</Button>
        </Link>
      </div>
    </Layout>
  );
};

export default NotFound;
