import React, { useEffect, useState } from 'react';
import { Spinner } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { getDrinkById } from '../../api';
import DrinkDetailsCard from '../../components/DrinkDetailsCard';
import Layout from '../../components/Layout';

const DrinkDetails = () => {
  const [loading, setLoading] = useState(false);
  const [details, setDetails] = useState(null);

  const { id } = useParams();

  useEffect(() => {
    // GET drink details
    const getDrinkDetails = async () => {
      setLoading(true);
      const drink = await getDrinkById(id);
      if (drink && drink.length > 0) {
        setDetails(drink[0]);
      }
      setLoading(false);
    };
    getDrinkDetails(id);
  }, [id]);

  return (
    <Layout>
      {loading && (
        <Spinner animation='border' role='status' className='mx-auto'>
          <span className='sr-only'>Loading...</span>
        </Spinner>
      )}
      {!loading && details && <DrinkDetailsCard details={details} />}
      {!loading && !details && <p>No drink with that ID</p>}
    </Layout>
  );
};

export default DrinkDetails;
