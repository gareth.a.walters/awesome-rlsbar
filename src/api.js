const DRINKS_URL = 'https://www.thecocktaildb.com/api/json/v1/1';
const INGREDIENT_IMAGE_URL = 'https://www.thecocktaildb.com/images/ingredients';

const objectAsParams = (obj) =>
  Object.keys(obj)
    .map((key) => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]);
    })
    .join('&');

// https://www.thecocktaildb.com/api.php

export const getRandomDrink = () =>
  fetch(`${DRINKS_URL}/random.php`)
    .then((r) => r.json())
    .then((d) => d.drinks[0]);

export const searchDrink = (i) =>
  fetch(`${DRINKS_URL}/search.php${objectAsParams({ i })}`)
    .then((r) => r.json())
    .then((d) => d.drinks[0]);

export const getCategories = () =>
  fetch(`${DRINKS_URL}/list.php?c=list`)
    .then((r) => r.json())
    .then((d) => d.drinks.map((e) => e.strCategory));

export const getIngredients = () =>
  fetch(`${DRINKS_URL}/list.php?i=list`)
    .then((r) => r.json())
    .then((d) => d.drinks.map((e) => e.strIngredient1));

export const getDrinkInCategory = (c) =>
  fetch(`${DRINKS_URL}/filter.php?c=${c}`)
    .then((r) => r.json())
    .then((d) => d.drinks);

export const getDrinksByLetter = (l) =>
  fetch(`${DRINKS_URL}/search.php?f=${l}`)
    .then((r) => r.json())
    .then((d) => d.drinks);

export const getDrinkById = (id) =>
  fetch(`${DRINKS_URL}/lookup.php?i=${id}`)
    .then((r) => r.json())
    .then((d) => d.drinks);

export const getIngredientByName = (name) =>
  fetch(`${DRINKS_URL}/search.php?i=${name}`)
    .then((r) => r.json())
    .then((d) => d.ingredients);

export const getIngredientImage = (name) =>
  fetch(`${INGREDIENT_IMAGE_URL}/${name}.png`).then((d) => d.url);

export const searchDrinksByIngredient = (name) =>
  fetch(`${DRINKS_URL}/filter.php?i=${name}`)
    .then((r) => r.json())
    .then((d) => d.drinks);

export const searchDrinkByName = (i) =>
  fetch(`${DRINKS_URL}/search.php?s=${i}`)
    .then((r) => r.json())
    .then((d) => d.drinks);
