export const saveTodosToStorage = (todos) => {
  localStorage.setItem('todos', JSON.stringify(todos));
};

export const loadTodosFromStorage = () => {
  const t = localStorage.getItem('todos');
  return JSON.parse(t);
};
